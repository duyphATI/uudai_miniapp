import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import BlueTickIcon from "../../icons/blue_tick";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";

function PreferentialPayingBillContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_special_healthcare_package}>
      <div className={styles.title_special_healthcare_package}>
        <p>Ưu đãi Thanh toán hóa đơn</p>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_special_healthcare_package}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_paying_bill/paying_bill.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_paying_bill/paying_bill_icon.png"
                  alt="Thanh toán hóa đơn"
                />
                <p>Thanh toán hóa đơn</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 20K</p>
                <p>Cho đơn từ 100k</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_paying_bill/data_3g_4g.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_paying_bill/data_3g_4g_icon.png"
                  alt="Data 3G/4G"
                />
                <p>Data 3G/4G</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 10%</p>
                <p>
                  Tối đa 10K cho đơn từ 20K khi mua/nạp Data 4G/5G Vinaphone
                </p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_paying_bill/momo.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_paying_bill/momo_icon.png"
                  alt="MIC"
                />
                <p>Momo</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 20K</p>
                <p>Cho hóa đơn đầu tiên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_paying_bill/paying_bill.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_paying_bill/paying_bill_icon.png"
                  alt="Thanh toán hóa đơn"
                />
                <p>Thanh toán hóa đơn</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 20K</p>
                <p>Cho đơn từ 100k</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_paying_bill/paying_bill.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_paying_bill/paying_bill_icon.png"
                  alt="Thanh toán hóa đơn"
                />
                <p>Thanh toán hóa đơn</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 20K</p>
                <p>Cho đơn từ 100k</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}
      </div>
    </div>
  );
}

export default PreferentialPayingBillContent;
