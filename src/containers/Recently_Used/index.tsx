import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";

function RecentlyUsedContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_recently_used}>
      <div className={styles.title_recently_used}>
        <p>Sử dụng gần đây</p>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_recently_used}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div className="d-flex align-items-center flex-column px-4">
          <div className={classnames(styles.card_recently_used)}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/recently_used/quiz_icon.png"
              />
            </Card>
          </div>
          <p>Quiz</p>
        </div>
        <div className="d-flex align-items-center flex-column px-4">
          <div className={styles.card_recently_used}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/recently_used/dices_icon.png"
              />
            </Card>
          </div>
          <p>Dices</p>
        </div>
        <div className="d-flex align-items-center flex-column px-4">
          <div className={styles.card_recently_used}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/recently_used/8_bit_icon.png"
              />
            </Card>
          </div>
          <p>8-Bit</p>
        </div>
        <div className="d-flex align-items-center flex-column px-4">
          <div className={styles.card_recently_used}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/recently_used/ve_gia_re_icon.png"
              />
            </Card>
          </div>
          <p>Vé giá rẻ</p>
        </div>
        <div className="d-flex align-items-center flex-column px-4">
          <div className={styles.card_recently_used}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/recently_used/di_mi_pay_icon.png"
              />
            </Card>
          </div>
          <p>DiMiPay</p>
        </div>
      </div>
    </div>
  );
}

export default RecentlyUsedContent;
