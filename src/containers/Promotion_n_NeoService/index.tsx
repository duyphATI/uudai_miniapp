import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import MoreIcon from "../../icons/more_icon";
import Grid from "@mui/material/Unstable_Grid2";

function PromotionAndNeoServiceContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_promotion_n_neoservice}>
      <div className={styles.title_promotion_n_neoservice}>
        <p>Khuyến mãi và dịch vụ mới</p>{" "}
        <div className={styles.suggest_text}>
          <div>Gợi ý</div>
        </div>
      </div>

      <Box sx={{ flexGrow: 1 }} className={styles.box_promotion_n_neoservice}>
        <Grid container>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={classnames(styles.card_promotion_n_neoservice)}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/promotion_n_neoservice/vietlott_icon.png"
                />
              </Card>
              <p>KM</p>
            </div>
            <p>Vietlott</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_promotion_n_neoservice}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/promotion_n_neoservice/flash_sale_icon.png"
                />
              </Card>
            </div>
            <p>Flash Sale</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_promotion_n_neoservice}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/promotion_n_neoservice/khach_san_icon.png"
                />
              </Card>
              <p>KM</p>
            </div>
            <p>Khách sạn</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_promotion_n_neoservice}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/promotion_n_neoservice/elsa_icon.png"
                />
              </Card>
            </div>
            <p>Tiếng Anh ELSA</p>
          </Grid>

          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_promotion_n_neoservice}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/promotion_n_neoservice/fpt_icon.png"
                />
              </Card>
            </div>
            <p>FPT</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_promotion_n_neoservice}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/promotion_n_neoservice/ve_xe_re_icon.png"
                />
              </Card>
              <p>KM</p>
            </div>
            <p>Vé Xe Rẻ</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_promotion_n_neoservice}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/promotion_n_neoservice/ve_may_bay_icon.png"
                />
              </Card>
              <p>KM</p>
            </div>
            <p>Vé máy bay</p>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}

export default PromotionAndNeoServiceContent;
