import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";

function PreferentialFromBrandContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_preferential_from_brand}>
      <div className={styles.title_preferential_from_brand}>
        <p>Ưu đãi từ thương hiệu</p>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_preferential_from_brand}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        {/* //=========================================================== */}
        <div
          className="d-flex align-items-center flex-column"
          style={{
            width: "66px",
            //, height: "60px"
          }}
        >
          <div className={classnames(styles.card_preferential_from_brand)}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/preferential_from_brand/yody_icon.png"
              />
            </Card>
          </div>
          <p>Yody</p>
        </div>

        {/* //=========================================================== */}

        <div
          className="d-flex align-items-center flex-column"
          style={{
            width: "66px",
            //, height: "60px"
          }}
        >
          <div className={styles.card_preferential_from_brand}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/preferential_from_brand/uniqlo_icon.png"
              />
            </Card>
          </div>
          <p>Uniqlo</p>
        </div>

        {/* //=========================================================== */}

        <div
          className="d-flex align-items-center flex-column"
          style={{
            width: "66px",
            //, height: "60px"
          }}
        >
          <div className={styles.card_preferential_from_brand}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/preferential_from_brand/kfc_icon.png"
              />
            </Card>
          </div>
          <p>KFC</p>
        </div>

        {/* //=========================================================== */}

        <div
          className="d-flex align-items-center flex-column"
          style={{
            width: "66px",
            //, height: "60px"
          }}
        >
          <div className={styles.card_preferential_from_brand}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/preferential_from_brand/nike_icon.png"
              />
            </Card>
          </div>
          <p>Nike</p>
        </div>

        {/* //=========================================================== */}

        <div
          className="d-flex align-items-center flex-column"
          style={{
            width: "66px",
            //, height: "60px"
          }}
        >
          <div className={styles.card_preferential_from_brand}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/preferential_from_brand/bobapop_icon.png"
              />
            </Card>
          </div>
          <p>Bobapop</p>
        </div>

          {/* //=========================================================== */}

          <div
          className="d-flex align-items-center flex-column"
          style={{
            width: "66px",
            //, height: "60px"
          }}
        >
          <div className={styles.card_preferential_from_brand}>
            <Card
              style={{ width: "40px", height: "40px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/preferential_from_brand/uniqlo_icon.png"
              />
            </Card>
          </div>
          <p>Uniqlo</p>
        </div>
      </div>
    </div>
  );
}

export default PreferentialFromBrandContent;
