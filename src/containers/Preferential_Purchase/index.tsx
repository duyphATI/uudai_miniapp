import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import BlueTickIcon from "../../icons/blue_tick";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";

function PreferentialPurchaseContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_preferential_purchase}>
      <div className={styles.title_preferential_purchase}>
        <p>Ưu đãi Mua sắm</p>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_preferential_purchase}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_preferential_purchase)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_purchase/yody.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_purchase/yody_icon.png"
                  alt="Yody"
                />
                <p>Yody</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Mua 1 tặng 1</p>
                <p>Tặng 1 áo POLO khi hóa đơn từ 1 triệu trở lên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_preferential_purchase)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_purchase/uniqlo.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_purchase/uniqlo_icon.png"
                  alt="Uniqlo"
                />
                <p>Uniqlo</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 50%</p>
                <p>Tối đa 200K</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_preferential_purchase)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_purchase/nike.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_purchase/nike_icon.png"
                  alt="MIC"
                />
                <p>Nike</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 200K</p>
                <p>Cho đơn từ 6000K</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_preferential_purchase)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_purchase/yody.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_purchase/yody_icon.png"
                  alt="Yody"
                />
                <p>Yody</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Mua 1 tặng 1</p>
                <p>Tặng 1 áo POLO khi hóa đơn từ 1 triệu trở lên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_preferential_purchase)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_purchase/yody.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_purchase/yody_icon.png"
                  alt="Yody"
                />
                <p>Yody</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Mua 1 tặng 1</p>
                <p>Tặng 1 áo POLO khi hóa đơn từ 1 triệu trở lên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}
      </div>
    </div>
  );
}

export default PreferentialPurchaseContent;
