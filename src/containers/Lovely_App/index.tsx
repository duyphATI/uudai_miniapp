import Box from "@mui/material/Box";
import Grid from "@mui/material/Unstable_Grid2";
import React from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";

function LovelyAppContent() {
  return (
    <div className={styles.container_lovely_app}>
      <div className={styles.title_lovely_app}>
        <p>Yêu thích</p>
        <p>Chỉnh sửa</p>
      </div>

      <Box sx={{ flexGrow: 1 }} className={styles.box_lovely_app}>
        <Grid container>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={classnames(styles.card_lovely_app)}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/lovely_app/grab_icon.png"
                />
              </Card>
            </div>
            <p>Grab</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_lovely_app}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/lovely_app/vietlott_icon.png"
                />
              </Card>

              <p>KM</p>
            </div>
            <p>Vietlott</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_lovely_app}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                {" "}
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/lovely_app/mobifone_icon.png"
                />
              </Card>

              <p>KM</p>
            </div>
            <p>Mobifone</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_lovely_app}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                {" "}
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/lovely_app/uber_icon.png"
                />
              </Card>
            </div>
            <p>Uber</p>
          </Grid>

          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_lovely_app}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                {" "}
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/lovely_app/vnpt_money_icon.png"
                />
              </Card>

              <p>KM</p>
            </div>
            <p>VNPT Money</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_lovely_app}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                {" "}
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/lovely_app/elsa_icon.png"
                />
              </Card>
            </div>
            <p>Tiếng anh ELSA</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_lovely_app}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                {" "}
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/lovely_app/momo_icon.png"
                />
              </Card>

              <p>KM</p>
            </div>
            <p>Momo</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_lovely_app}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/lovely_app/data_4g_icon.png"
                />
              </Card>
            </div>
            <p>Data 3G/4G</p>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}

export default LovelyAppContent;
