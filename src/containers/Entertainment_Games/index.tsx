import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import MoreIcon from "../../icons/more_icon";

function EntertainmentGamesContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_entertainment_games}>
      <div className={styles.title_entertainment_games}>
        <p>Trò chơi giải trí</p>{" "}
        <div className={styles.suggest_text}>
          <div>Gợi ý</div>
        </div>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.all_game_container_list}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div
          className={styles.box_entertainment_games}
          style={{
            width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
          }}
        >
          <div
            className="d-flex align-items-start justify-content-start  mb-3 "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={classnames(styles.card_entertainment_games)}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/entertainment_games/garena_lienquan.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Garena Liên Quân Mobile</p>
              <p>
                Liên Quân Mobile là một trong những game nhập vai trực tuyến có
                nhiều người chơi nhất hiện nay. Game có đồ họa và âm thanh thực,
                chất lượng đến từng chi tiết , được phát hành bởi Garena, người
                dùng có thể trải nghiệm game trên các nền tảng điện thoại di
                động Android, iOS và Nintendo Switch.
              </p>
              <p>Trò chơi | + 100k người dùng</p>
            </div>
          </div>
          <div
            className="d-flex align-items-start justify-content-start  mb-3  "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={styles.card_entertainment_games}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/entertainment_games/dautruongchanly.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Đấu Trường Chân Lý </p>
              <p>
                Đấu Trường Chân Lý hay ĐTCL là một chế độ chơi của Liên Minh
                Huyền Thoại, được phát triển bởi Riot Games, lấy cảm hứng từ màn
                chơi
              </p>
              <p>Trò chơi | + 100k người dùng</p>
            </div>
          </div>

          <div
            className="d-flex align-items-start justify-content-start  mb-3  "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={styles.card_entertainment_games}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/entertainment_games/among_us.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Among US</p>
              <p>
                Cơn sốt Among Us năm 2021 dường như vẫn chưa nguội đi khi các
                cộng đồng yêu thích chơi game này vẫn còn lớn mạnh. Trò chơi
                Among US được lấy bối cảnh bên ngoài vũ trụ, người chơi sẽ hóa
                thân thành các nhân vật là Crewmates
              </p>
              <p>Trò chơi | + 100k người dùng</p>
            </div>
          </div>
        </div>

        <div
          className={styles.box_entertainment_games}
          style={{
            width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
          }}
        >
          <div
            className="d-flex align-items-start justify-content-start  mb-3 "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={classnames(styles.card_entertainment_games)}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/entertainment_games/pubg.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Uber</p>
              <p>
                Uber là một công ty đa quốc gia của Mỹ cung cấp các dịch vụ giao
                thông vận tải thông qua một ứng dụng công nghệ. 
              </p>
              <p>Tiện ích | + 100k người dùng </p>
            </div>
          </div>

          <div
            className="d-flex align-items-start justify-content-start  mb-3  "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={styles.card_entertainment_games}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/entertainment_games/tocchien.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Grab</p>
              <p>
                Grab là ứng dụng đặt xe tiện lợi được yêu chuộng nhất Đông Nam Á
                với mục đích mang đến cho người dùng các dịch vụ vận chuyển
                nhanh chóng, bằng cách kết nối hơn 10 triệu hành
              </p>
              <p>Tiện ích | + 100k người dùng</p>
            </div>
          </div>

          <div
            className="d-flex align-items-start justify-content-start  mb-3  "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={styles.card_entertainment_games}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/entertainment_games/super_snail.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Be</p>
              <p>
                Be là ứng dụng gọi xe công nghệ được phát triển bởi Công ty cổ
                phần Be GROUP, startup công nghệ Việt Nam, 
              </p>
              <p>Tiện ích | + 100k người dùng</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EntertainmentGamesContent;
