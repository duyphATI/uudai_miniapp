import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import BlueTickIcon from "../../icons/blue_tick";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";

function PreferentialFoodDrinkContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_special_healthcare_package}>
      <div className={styles.title_special_healthcare_package}>
        <p>Ưu đãi Ăn uống</p>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_special_healthcare_package}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_food_drink/gong_cha.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_food_drink/gong_cha_icon.png"
                  alt="Gong Cha"
                />
                <p>Gong Cha</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Mua 1 tặng 1</p>
                <p>Tặng 1 trà sữa bánh quy gừng khi mua kèm món nước bất kì</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_food_drink/tocotoco.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_food_drink/tocotoco_icon.png"
                  alt="ToCoToCo"
                />
                <p>ToCoToCo</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 50%</p>
                <p>Tối đa 20K</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_food_drink/bobapop.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_food_drink/bobapop_icon.png"
                  alt="MIC"
                />
                <p>Bobapop</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 20K</p>
                <p>Cho đơn từ 60K</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_food_drink/gong_cha.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_food_drink/gong_cha_icon.png"
                  alt="Gong Cha"
                />
                <p>Gong Cha</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Mua 1 tặng 1</p>
                <p>Tặng 1 trà sữa bánh quy gừng khi mua kèm món nước bất kì</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/preferential_food_drink/gong_cha.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/preferential_food_drink/gong_cha_icon.png"
                  alt="Gong Cha"
                />
                <p>Gong Cha</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Mua 1 tặng 1</p>
                <p>Tặng 1 trà sữa bánh quy gừng khi mua kèm món nước bất kì</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}
      </div>
    </div>
  );
}

export default PreferentialFoodDrinkContent;
