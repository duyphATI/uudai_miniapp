import Box from "@mui/material/Box";
import Grid from "@mui/material/Unstable_Grid2";
import React from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import TipIcon from "../../icons/tip_icon";

function TelecomDataCardGameContent() {
  return (
    <div className={styles.container_telecom_data_cardgame}>
      <div className={styles.title_telecom_data_cardgame}>
        <p>Viễn thông - Data - Thẻ game</p>
        <p>Xem tất cả</p>
      </div>

      <Box sx={{ flexGrow: 1 }} className={styles.box_telecom_data_cardgame}>
        <Grid container>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={classnames(styles.card_telecom_data_cardgame)}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/telecom_data_cardgame/viettel_money_icon.png"
                />
              </Card>

              <p>KM</p>
            </div>
            <p>Viettel Money</p>
          </Grid>

          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_telecom_data_cardgame}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/telecom_data_cardgame/zalo_pay_icon.png"
                />
              </Card>
            </div>
            <p>Zalopay</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_telecom_data_cardgame}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/telecom_data_cardgame/momo_icon.png"
                />
              </Card>
            </div>
            <p>Momo</p>
          </Grid>

          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_telecom_data_cardgame}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/telecom_data_cardgame/vinaphone_icon.png"
                />
              </Card>

              <p>KM</p>
            </div>
            <p>Vinaphone</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_telecom_data_cardgame}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/telecom_data_cardgame/mobifone_icon.png"
                />
              </Card>
            </div>
            <p>Mobifone</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_telecom_data_cardgame}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/telecom_data_cardgame/vnpt_icon.png"
                />
              </Card>

              <p>KM</p>
            </div>
            <p>VNPT</p>
          </Grid>
          <Grid xs={3} className="d-flex align-items-center flex-column">
            <div className={styles.card_telecom_data_cardgame}>
              <Card
                style={{ width: "40px", height: "40px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="40"
                  height="40"
                  image="/images/telecom_data_cardgame/data_3g_4g_icon.png"
                />
              </Card>

              <p>KM</p>
            </div>
            <p>Data 3G/4G</p>
          </Grid>
        </Grid>
      </Box>

      <div className={styles.tip_content}>
        <div>
          <TipIcon />
        </div>
        <p>
          Mẹo: Bạn có thể tải các tiện ích xuống như ứng dụng để sử dụng hoặc sử
          dụng trực tiếp trên miniapp
        </p>
      </div>
    </div>
  );
}

export default TelecomDataCardGameContent;
