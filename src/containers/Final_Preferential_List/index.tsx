import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import React, { useMemo } from "react";
import { useWindowSize } from "usehooks-ts";

import styles from "./index.module.scss";
import AllList from "./AllList";
import NewestList from "./NewestList";

function FinalPreferentialListContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  const [value, setValue] = React.useState("1");

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
  };

  return (
    <div className={styles.container_financial_n_insurance}>
      <Box sx={{ width: "100%", typography: "body1" }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList onChange={handleChange} aria-label="lab API tabs example">
              <Tab label="Tất cả" value="1" />
              <Tab label="Mới nhất" value="2" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <AllList widthForListItem={widthForListItem} styles={styles} />
          </TabPanel>
          <TabPanel value="2">
            <NewestList widthForListItem={widthForListItem} styles={styles} />
          </TabPanel>
        </TabContext>
      </Box>
    </div>
  );
}

export default FinalPreferentialListContent;
