import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import classnames from "classnames";
import React, { useMemo } from "react";
import BlueTickIcon from "../../icons/blue_tick";

type PropsType = {
  widthForListItem: any;
  styles: any;
};

export default function NewestList({ widthForListItem, styles }: PropsType) {
  return (
    <div
      className={styles.box_final_preferential_list}
      style={{
        maxWidth: widthForListItem,
        overflow: "auto",
      }}
    >
      {/* //==================================================================================== */}
      <div className="d-flex align-items-start justify-content-start w-100 mb-3 ">
        <div className={classnames(styles.card_final_preferential_list)}>
          <Card
            style={{ width: "100px", height: "100px", borderRadius: "4px" }}
          >
            <CardMedia
              component="img"
              width="100"
              height="100"
              image="/images/final_preferential_list/pay_bill.png"
            />
          </Card>
        </div>
        <div className={classnames(styles.content_item_list, "ps-2")}>
          <div className={styles.card_item_title}>
            <img
              src="/images/final_preferential_list/pay_bill_icon.png"
              alt="Pay Bill"
            />
            <p>Thanh toán hóa đơn</p>
            {/* <BlueTickIcon></BlueTickIcon> */}
          </div>

          <div className={styles.card_item_content}>
            <p>Giảm 20K</p>
            <p>Cho đơn từ 100k</p>
          </div>

          <div className="d-flex align-items-end justify-content-end w-100 flex-grow-1">
            <Button
              variant="outlined"
              style={{
                textTransform: "unset",
                padding: "4px 8px 4px 8px",
              }}
            >
              <span
                style={{
                  fontFamily: "Inter",
                  fontSize: "12px",
                  fontStyle: "normal",
                  fontWeight: "500",
                  lineHeight: "16px",
                }}
              >
                Thu thập
              </span>
            </Button>
          </div>
        </div>
      </div>
      {/* //==================================================================================== */}

      {/* //==================================================================================== */}
      <div className="d-flex align-items-start justify-content-start w-100 mb-3 ">
        <div className={classnames(styles.card_final_preferential_list)}>
          <Card
            style={{ width: "100px", height: "100px", borderRadius: "4px" }}
          >
            <CardMedia
              component="img"
              width="100"
              height="100"
              image="/images/final_preferential_list/data_4g.png"
            />
          </Card>
        </div>
        <div className={classnames(styles.content_item_list, "ps-2")}>
          <div className={styles.card_item_title}>
            <img
              src="/images/final_preferential_list/data_4g_icon.png"
              alt="Data 3G/4G"
            />
            <p>Data 3G/4G</p>
            <BlueTickIcon></BlueTickIcon>
          </div>

          <div className={styles.card_item_content}>
            <p>Giảm 10%</p>
            <p>Tối đa 10K cho đơn từ 20K khi mua/nạp Data 4G/5G Vinaphone</p>
          </div>

          <div className="d-flex align-items-end justify-content-end w-100 flex-grow-1">
            <Button
              variant="outlined"
              style={{
                textTransform: "unset",
                padding: "4px 8px 4px 8px",
              }}
            >
              <span
                style={{
                  fontFamily: "Inter",
                  fontSize: "12px",
                  fontStyle: "normal",
                  fontWeight: "500",
                  lineHeight: "16px",
                }}
              >
                Thu thập
              </span>
            </Button>
          </div>
        </div>
      </div>
      {/* //==================================================================================== */}

      {/* //==================================================================================== */}
      <div className="d-flex align-items-start justify-content-start w-100 mb-3 ">
        <div className={classnames(styles.card_final_preferential_list)}>
          <Card
            style={{ width: "100px", height: "100px", borderRadius: "4px" }}
          >
            <CardMedia
              component="img"
              width="100"
              height="100"
              image="/images/final_preferential_list/gong_cha.png"
            />
          </Card>
        </div>
        <div className={classnames(styles.content_item_list, "ps-2")}>
          <div className={styles.card_item_title}>
            <img
              src="/images/final_preferential_list/gong_cha_icon.png"
              alt="Gong Cha"
            />
            <p>Gong Cha</p>
            <BlueTickIcon></BlueTickIcon>
          </div>

          <div className={styles.card_item_content}>
            <p>Mua 1 tặng 1</p>
            <p>Tặng 1 trà sữa bánh quy gừng khi mua kèm món nước bất kì</p>
          </div>

          <div className="d-flex align-items-end justify-content-end w-100">
            <Button
              variant="outlined"
              style={{
                textTransform: "unset",
                padding: "4px 8px 4px 8px",
              }}
            >
              <span
                style={{
                  fontFamily: "Inter",
                  fontSize: "12px",
                  fontStyle: "normal",
                  fontWeight: "500",
                  lineHeight: "16px",
                }}
              >
                Thu thập
              </span>
            </Button>
          </div>
        </div>
      </div>
      {/* //==================================================================================== */}

      {/* //==================================================================================== */}
      <div className="d-flex align-items-start justify-content-start w-100 mb-3 ">
        <div className={classnames(styles.card_final_preferential_list)}>
          <Card
            style={{ width: "100px", height: "100px", borderRadius: "4px" }}
          >
            <CardMedia
              component="img"
              width="100"
              height="100"
              image="/images/final_preferential_list/tocotoco.png"
            />
          </Card>
        </div>
        <div className={classnames(styles.content_item_list, "ps-2")}>
          <div className={styles.card_item_title}>
            <img
              src="/images/final_preferential_list/tocotoco_icon.png"
              alt="ToCoToCo"
            />
            <p>ToCoToCo</p>
            <BlueTickIcon></BlueTickIcon>
          </div>

          <div className={styles.card_item_content}>
            <p>Giảm 50%</p>
            <p>Tối đa 20K</p>
          </div>

          <div className="d-flex align-items-end justify-content-end w-100 flex-grow-1">
            <Button
              variant="outlined"
              style={{
                textTransform: "unset",
                padding: "4px 8px 4px 8px",
              }}
            >
              <span
                style={{
                  fontFamily: "Inter",
                  fontSize: "12px",
                  fontStyle: "normal",
                  fontWeight: "500",
                  lineHeight: "16px",
                }}
              >
                Thu thập
              </span>
            </Button>
          </div>
        </div>
      </div>
      {/* //==================================================================================== */}

      {/* //==================================================================================== */}
      <div className="d-flex align-items-start justify-content-start w-100 mb-3 ">
        <div className={classnames(styles.card_final_preferential_list)}>
          <Card
            style={{ width: "100px", height: "100px", borderRadius: "4px" }}
          >
            <CardMedia
              component="img"
              width="100"
              height="100"
              image="/images/final_preferential_list/bobapop.png"
            />
          </Card>
        </div>
        <div className={classnames(styles.content_item_list, "ps-2")}>
          <div className={styles.card_item_title}>
            <img
              src="/images/final_preferential_list/bobapop_icon.png"
              alt="Bobapop"
            />
            <p>Bobapop</p>
            <BlueTickIcon></BlueTickIcon>
          </div>

          <div className={styles.card_item_content}>
            <p>Giảm 20K</p>
            <p>Cho đơn từ 60K</p>
          </div>

          <div className="d-flex align-items-end justify-content-end w-100 flex-grow-1">
            <Button
              variant="outlined"
              style={{
                textTransform: "unset",
                padding: "4px 8px 4px 8px",
              }}
            >
              <span
                style={{
                  fontFamily: "Inter",
                  fontSize: "12px",
                  fontStyle: "normal",
                  fontWeight: "500",
                  lineHeight: "16px",
                }}
              >
                Thu thập
              </span>
            </Button>
          </div>
        </div>
      </div>
      {/* //==================================================================================== */}

      {/* //==================================================================================== */}
      <div className="d-flex align-items-start justify-content-start w-100 mb-3 ">
        <div className={classnames(styles.card_final_preferential_list)}>
          <Card
            style={{ width: "100px", height: "100px", borderRadius: "4px" }}
          >
            <CardMedia
              component="img"
              width="100"
              height="100"
              image="/images/final_preferential_list/pay_bill.png"
            />
          </Card>
        </div>
        <div className={classnames(styles.content_item_list, "ps-2")}>
          <div className={styles.card_item_title}>
            <img
              src="/images/final_preferential_list/pay_bill_icon.png"
              alt="Pay Bill"
            />
            <p>Thanh toán hóa đơn</p>
            {/* <BlueTickIcon></BlueTickIcon> */}
          </div>

          <div className={styles.card_item_content}>
            <p>Giảm 20K</p>
            <p>Cho đơn từ 100k</p>
          </div>

          <div className="d-flex align-items-end justify-content-end w-100 flex-grow-1">
            <Button
              variant="outlined"
              style={{
                textTransform: "unset",
                padding: "4px 8px 4px 8px",
              }}
            >
              <span
                style={{
                  fontFamily: "Inter",
                  fontSize: "12px",
                  fontStyle: "normal",
                  fontWeight: "500",
                  lineHeight: "16px",
                }}
              >
                Thu thập
              </span>
            </Button>
          </div>
        </div>
      </div>
      {/* //==================================================================================== */}
    </div>
  );
}
