import Box from "@mui/material/Box";
import Grid from "@mui/material/Unstable_Grid2";
import React from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import BlueTickIcon from "../../icons/blue_tick";
import Button from "@mui/material/Button";
import MoreIcon from "../../icons/more_icon";

function PreferentialTravelingContent() {
  return (
    <div className={styles.container_preferential_traveling}>
      <div className={styles.title_preferential_traveling}>
        <p>Ưu đãi Du lịch vi vu</p>
        <p>Xem tất cả</p>
      </div>

      <Box className={styles.box_preferential_traveling}>
        <Grid container spacing={1.5}>
          {/* //============================================================= */}
          <Grid xs={6} className="d-flex align-items-center flex-column">
            <div className={classnames(styles.card_special_healthcare_package)}>
              <Card style={{ borderRadius: "4px", width: "100%" }}>
                <CardMedia
                  component="img"
                  width="140"
                  height="120"
                  image="/images/preferential_traveling/mixivivu.png"
                />
                <div className={styles.card_item_title}>
                  <img
                    src="/images/preferential_traveling/mixivivu_icon.png"
                    alt="Mixivivu"
                  />
                  <p>Mixivivu</p>
                  <BlueTickIcon></BlueTickIcon>
                </div>

                <div className={styles.card_item_content}>
                  <p>Giảm 200K</p>
                  <p>Cho lần đầu tiên mua vé</p>
                </div>

                <div className="d-flex align-items-center justify-content-end ">
                  <Button
                    variant="outlined"
                    style={{
                      textTransform: "unset",
                      padding: "4px 8px 4px 8px",
                    }}
                  >
                    <span
                      style={{
                        fontFamily: "Inter",
                        fontSize: "12px",
                        fontStyle: "normal",
                        fontWeight: "500",
                        lineHeight: "16px",
                      }}
                    >
                      Thu thập
                    </span>
                  </Button>
                </div>
              </Card>
            </div>
          </Grid>

          {/* //============================================================= */}

          <Grid xs={6} className="d-flex align-items-center flex-column">
            <div className={classnames(styles.card_special_healthcare_package)}>
              <Card style={{ borderRadius: "4px" }}>
                <CardMedia
                  component="img"
                  width="140"
                  height="120"
                  image="/images/preferential_traveling/traveloka.png"
                />
                <div className={styles.card_item_title}>
                  <img
                    src="/images/preferential_traveling/traveloka_icon.png"
                    alt="Traveloka"
                  />
                  <p>Traveloka</p>
                  <BlueTickIcon></BlueTickIcon>
                </div>

                <div className={styles.card_item_content}>
                  <p>Giảm đến 50%</p>
                  <p>Khi mua vé du lịch quốc tế </p>
                </div>

                <div className="d-flex align-items-center justify-content-end ">
                  <Button
                    variant="outlined"
                    style={{
                      textTransform: "unset",
                      padding: "4px 8px 4px 8px",
                    }}
                  >
                    <span
                      style={{
                        fontFamily: "Inter",
                        fontSize: "12px",
                        fontStyle: "normal",
                        fontWeight: "500",
                        lineHeight: "16px",
                      }}
                    >
                      Thu thập
                    </span>
                  </Button>
                </div>
              </Card>
            </div>
          </Grid>

          <Grid xs={6} className="d-flex align-items-center flex-column">
            <div className={classnames(styles.card_special_healthcare_package)}>
              <Card style={{ borderRadius: "4px" }}>
                <CardMedia
                  component="img"
                  width="140"
                  height="120"
                  image="/images/preferential_traveling/metrip.png"
                />
                <div className={styles.card_item_title}>
                  <img
                    src="/images/preferential_traveling/metrip_icon.png"
                    alt="MeTrip"
                  />
                  <p>MeTrip</p>
                  <BlueTickIcon></BlueTickIcon>
                </div>

                <div className={styles.card_item_content}>
                  <p>Giảm đến 50%</p>
                  <p>Khi mua vé du lịch quốc tế </p>
                </div>

                <div className="d-flex align-items-center justify-content-end ">
                  <Button
                    variant="outlined"
                    style={{
                      textTransform: "unset",
                      padding: "4px 8px 4px 8px",
                    }}
                  >
                    <span
                      style={{
                        fontFamily: "Inter",
                        fontSize: "12px",
                        fontStyle: "normal",
                        fontWeight: "500",
                        lineHeight: "16px",
                      }}
                    >
                      Thu thập
                    </span>
                  </Button>
                </div>
              </Card>
            </div>
          </Grid>

          <Grid xs={6} className="d-flex align-items-center flex-column">
            <div className={classnames(styles.card_special_healthcare_package)}>
              <Card style={{ borderRadius: "4px" }}>
                <CardMedia
                  component="img"
                  width="140"
                  height="120"
                  image="/images/preferential_traveling/alotrip.png"
                />
                <div className={styles.card_item_title}>
                  <img
                    src="/images/preferential_traveling/alotrip_icon.png"
                    alt="AloTrip"
                  />
                  <p>AloTrip</p>
                  <BlueTickIcon></BlueTickIcon>
                </div>

                <div className={styles.card_item_content}>
                  <p>Giảm 200K</p>
                  <p>Cho lần đầu tiên mua vé</p>
                </div>

                <div className="d-flex align-items-center justify-content-end ">
                  <Button
                    variant="outlined"
                    style={{
                      textTransform: "unset",
                      padding: "4px 8px 4px 8px",
                    }}
                  >
                    <span
                      style={{
                        fontFamily: "Inter",
                        fontSize: "12px",
                        fontStyle: "normal",
                        fontWeight: "500",
                        lineHeight: "16px",
                      }}
                    >
                      Thu thập
                    </span>
                  </Button>
                </div>
              </Card>
            </div>
          </Grid>
        </Grid>
      </Box>

      <div className={styles.more_text}>
        <div className={styles.icon_button}>
          <MoreIcon></MoreIcon>
          <span>Xem thêm</span>
        </div>
      </div>
    </div>
  );
}

export default PreferentialTravelingContent;
