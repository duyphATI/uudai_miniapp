import { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import { useWindowSize } from "usehooks-ts";
import DotIcon from "../../icons/dot";
import styles from "./index.module.scss";

function VoucherCouponContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_voucher_coupon}>
      {/* //===================================================================== */}
      <div
        className={classnames(
          styles.voucher_coupon_double,
          "d-flex align-items-start justify-content-between"
        )}
      >
        <div
          className="d-flex align-items-start justify-content-start w-100 pe-4 "
          style={{ borderRight: "1px solid rgba(136, 143, 151, 0.2)" }}
        >
          <div className={classnames(styles.card_voucher_coupon)}>
            <Card
              style={{ width: "32px", height: "32px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="32"
                height="32"
                image="/images/voucher_coupon/coupon_1.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "ps-2")}>
            <div className={styles.card_item_content}>
              <p>Nhập mã</p>
              <p>Mã ưu đãi, mã giới thiệu</p>
            </div>
          </div>
        </div>

        <div className="d-flex align-items-start justify-content-start w-100  ps-4">
          <div className={classnames(styles.card_voucher_coupon)}>
            <Card
              style={{ width: "32px", height: "32px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="32"
                height="32"
                image="/images/voucher_coupon/coupon_2.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "ps-2")}>
            <div className={styles.card_item_content}>
              <p>Quà của bạn</p>
              <p>
                Có 17 ưu đãi &nbsp;<DotIcon></DotIcon>
              </p>
            </div>
          </div>
        </div>
      </div>
      {/* //===================================================================== */}

      <div
        className={styles.box_voucher_coupon}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div
          className="d-flex align-items-center flex-column "
          style={{
            width: "64px",
            // height: "60px"
          }}
        >
          <div className={classnames(styles.card_voucher_coupon)}>
            <Card
              style={{
                width: "40px",
                height: "40px",
                borderRadius: "4px",
                background: "#FFDFDF",
                boxShadow: " 0px 0px 28px 0px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/voucher_coupon/voucher_icon_1.png"
              />
            </Card>
          </div>
          <p>Sức khỏe Y tế</p>
        </div>

        {/* //================================================================= */}
        <div
          className="d-flex align-items-center flex-column "
          style={{
            width: "64px",
            // height: "60px"
          }}
        >
          <div className={styles.card_voucher_coupon}>
            <Card
              style={{
                width: "40px",
                height: "40px",
                borderRadius: "4px",
                background: "#FFF2DF",
                boxShadow: " 0px 0px 28px 0px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/voucher_coupon/voucher_icon_2.png"
              />
            </Card>
          </div>
          <p>Mua sắm</p>
        </div>
        {/* //================================================================= */}

        <div
          className="d-flex align-items-center flex-column "
          style={{
            width: "64px",
            // height: "60px"
          }}
        >
          <div className={styles.card_voucher_coupon}>
            <Card
              style={{
                width: "40px",
                height: "40px",
                borderRadius: "4px",
                background: "#E2FFDF",
                boxShadow: " 0px 0px 28px 0px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/voucher_coupon/voucher_icon_3.png"
              />
            </Card>
          </div>
          <p>Thanh toán hóa đơn</p>
        </div>

        {/* //================================================================= */}
        <div
          className="d-flex align-items-center flex-column "
          style={{
            width: "64px",
            // height: "60px"
          }}
        >
          <div className={styles.card_voucher_coupon}>
            <Card
              style={{
                width: "40px",
                height: "40px",
                borderRadius: "4px",
                background: "#DFF2FF",
                boxShadow: " 0px 0px 28px 0px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/voucher_coupon/voucher_icon_4.png"
              />
            </Card>
          </div>
          <p>Du lịch & Đi lại</p>
        </div>
        {/* //================================================================= */}

        {/* //================================================================= */}
        <div
          className="d-flex align-items-center flex-column "
          style={{
            width: "64px",
            // height: "60px"
          }}
        >
          <div className={styles.card_voucher_coupon}>
            <Card
              style={{
                width: "40px",
                height: "40px",
                borderRadius: "4px",
                background: "#E0DFFF",
                boxShadow: " 0px 0px 28px 0px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/voucher_coupon/voucher_icon_5.png"
              />
            </Card>
          </div>
          <p>Giải trí</p>
        </div>
        {/* //================================================================= */}

        {/* //================================================================= */}
        <div
          className="d-flex align-items-center flex-column "
          style={{
            width: "64px",
            // height: "60px"
          }}
        >
          <div className={styles.card_voucher_coupon}>
            <Card
              style={{
                width: "40px",
                height: "40px",
                borderRadius: "4px",
                background: "#DFF2FF",
                boxShadow: " 0px 0px 28px 0px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardMedia
                component="img"
                width="40"
                height="40"
                image="/images/voucher_coupon/voucher_icon_6.png"
              />
            </Card>
          </div>
          <p>Cơ quan nhà nước</p>
        </div>
        {/* //================================================================= */}
      </div>
    </div>
  );
}

export default VoucherCouponContent;
