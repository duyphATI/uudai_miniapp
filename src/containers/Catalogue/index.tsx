import { useMemo } from "react";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import classnames from "classnames";

function CatalogueContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_catalogue}>
      <div className={styles.title_catalogue}>
        <p>Danh mục</p>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_catalogue}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div className={styles.catalogue_list_content}>
          <div className="d-flex flex-column">
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_1_a
              )}
            >
              <div>
                <p>Tài chính, bảo hiểm</p>
              </div>
              <img
                alt="Tài chính, bảo hiểm"
                src="/images/catalogue/taichinh_baohiem.svg"
              />
            </div>
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_1_b
              )}
            >
              <div>
                <p>Trò chơi</p>
              </div>
              <img alt="Trò chơi" src="/images/catalogue/trochoi.svg" />
            </div>
          </div>
          <div className="d-flex flex-column">
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_2_a
              )}
            >
              <div>
                <p>Khuyến mại</p>
              </div>
              <img alt="Khuyến mại" src="/images/catalogue/khuyenmai.svg" />
            </div>
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_2_b
              )}
            >
              <div>
                <p>Sức khỏe - y tế</p>
              </div>
              <img alt="Trò chơi" src="/images/catalogue/suckhoe_yte.svg" />
            </div>
          </div>
          <div className="d-flex flex-column">
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_3_a
              )}
            >
              <div>
                <p>Mua sắm</p>
              </div>
              <img alt="Mua sắm" src="/images/catalogue/muasam.svg" />
            </div>
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_3_b
              )}
            >
              <div>
                <p>Du lịch</p>
              </div>
              <img alt="Du lịch" src="/images/catalogue/dulich.svg" />
            </div>
          </div>
          <div className="d-flex flex-column">
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_4_a
              )}
            >
              <div>
                <p>Thanh toán hóa đơn</p>
              </div>
              <img
                alt="Thanh toán hóa đơn"
                src="/images/catalogue/thanhtoanhoadon.svg"
              />
            </div>
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_4_b
              )}
            >
              <div>
                <p>Tiện ích</p>
              </div>
              <img alt="Tiện ích" src="/images/catalogue/tienich.svg" />
            </div>
          </div>
          <div className="d-flex flex-column">
            <div
              className={classnames(
                styles.content_item_catalog,
                styles.card_catalogue_5_a
              )}
            >
              <div>
                <p>Cơ quan nhà nước</p>
              </div>
              <img
                alt="Cơ quan nhà nước"
                src="/images/catalogue/coquannhanuoc.svg"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CatalogueContent;
