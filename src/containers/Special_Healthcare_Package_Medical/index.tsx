import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import BlueTickIcon from "../../icons/blue_tick";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";

function SpecialHealthcarePackageMedicalContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_special_healthcare_package}>
      <div className={styles.title_special_healthcare_package}>
        <p>Ưu đãi Gói sức khỏe - y tế</p>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_special_healthcare_package}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/special_healthcare_package_medical/insurance_manulife.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/special_healthcare_package_medical/manulife_icon.png"
                  alt="Manulife"
                />
                <p>Bảo hiểm Manulife</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 20%</p>
                <p>Tối đa 1 triệu cho các gói 10 triệu trở lên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/special_healthcare_package_medical/medlatec.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/special_healthcare_package_medical/medlatec_icon.png"
                  alt="Medlatec"
                />
                <p>Medlatec</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 50%</p>
                <p>Tối đa 1 triệu cho các gói 10 triệu trở lên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/special_healthcare_package_medical/insurance_mic.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/special_healthcare_package_medical/mic_icon.png"
                  alt="MIC"
                />
                <p>Bảo hiểm MIC</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 10%</p>
                <p>Tối đa 1 triệu cho các gói 10 triệu trở lên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/special_healthcare_package_medical/insurance_manulife.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/special_healthcare_package_medical/manulife_icon.png"
                  alt="Manulife"
                />
                <p>Bảo hiểm Manulife</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 20%</p>
                <p>Tối đa 1 triệu cho các gói 10 triệu trở lên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}

        {/* //================================================================== */}
        <div className="d-flex align-items-center flex-column ">
          <div className={classnames(styles.card_special_healthcare_package)}>
            <Card style={{ width: "140px", borderRadius: "4px" }}>
              <CardMedia
                component="img"
                width="140"
                height="120"
                image="/images/special_healthcare_package_medical/insurance_manulife.png"
              />
              <div className={styles.card_item_title}>
                <img
                  src="/images/special_healthcare_package_medical/manulife_icon.png"
                  alt="Manulife"
                />
                <p>Bảo hiểm Manulife</p>
                <BlueTickIcon></BlueTickIcon>
              </div>

              <div className={styles.card_item_content}>
                <p>Giảm 20%</p>
                <p>Tối đa 1 triệu cho các gói 10 triệu trở lên</p>
              </div>

              <div className="d-flex align-items-center justify-content-end ">
                <Button
                  variant="outlined"
                  style={{
                    textTransform: "unset",
                    padding: "4px 8px 4px 8px",
                  }}
                >
                  <span
                    style={{
                      fontFamily: "Inter",
                      fontSize: "12px",
                      fontStyle: "normal",
                      fontWeight: "500",
                      lineHeight: "16px",
                    }}
                  >
                    Thu thập
                  </span>
                </Button>
              </div>
            </Card>
          </div>
        </div>
        {/* //================================================================== */}
      </div>
    </div>
  );
}

export default SpecialHealthcarePackageMedicalContent;
