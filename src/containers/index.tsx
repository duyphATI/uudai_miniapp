import React from "react";

import FirstPreferentialSlickContent from "./First_Preferential_Slick";
import LovelyAppContent from "./Lovely_App";
import SpecialHealthcarePackageMedicalContent from "./Special_Healthcare_Package_Medical";
import PreferentialFoodDrinkContent from "./Preferential_Food_Drink";
import TheFollowingBrandContent from "./The_Following_Brand";
import PreferentialPurchaseContent from "./Preferential_Purchase";
import PreferentialFromBrandContent from "./Preferential_From_Brand";
import SecondPreferentialSlickContent from "./Second_Preferential_Slick";
import PreferentialPayingBillContent from "./Preferential_Paying_Bill";
import PreferentialTravelingContent from "./Preferential_Traveling";
import FinalPreferentialListContent from "./Final_Preferential_List";
import VoucherCouponContent from "./Voucher_Coupon";

function ContainerContent() {
  return (
    <React.Fragment>
      <FirstPreferentialSlickContent></FirstPreferentialSlickContent>

      <VoucherCouponContent></VoucherCouponContent>

      <SpecialHealthcarePackageMedicalContent></SpecialHealthcarePackageMedicalContent>

      <PreferentialFoodDrinkContent></PreferentialFoodDrinkContent>

      <TheFollowingBrandContent></TheFollowingBrandContent>

      <PreferentialPurchaseContent></PreferentialPurchaseContent>

      <PreferentialFromBrandContent></PreferentialFromBrandContent>

      <SecondPreferentialSlickContent></SecondPreferentialSlickContent>

      <PreferentialPayingBillContent></PreferentialPayingBillContent>

      <PreferentialTravelingContent></PreferentialTravelingContent>

      <FinalPreferentialListContent></FinalPreferentialListContent>

      {/*    <DeliverAndBookingContent></DeliverAndBookingContent>

      <PurchaseContent></PurchaseContent>

      <HealthContent></HealthContent>

      <UtilityContent></UtilityContent>

      <SuggestOfficialAccountContent></SuggestOfficialAccountContent> */}
    </React.Fragment>
  );
}

export default ContainerContent;
