import React from "react";
import ContainerContent from "../containers";
import Box from "@mui/material/Box";

function Main() {
  return (
    <div className="my-4">
      {/* <div>MiniApp</div> */}
      <ContainerContent></ContainerContent>
    </div>
  );
}

export default Main;
