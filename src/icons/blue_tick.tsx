import * as React from "react";
import SvgIcon from "@mui/material/SvgIcon";

export default function BlueTickIcon() {
  return (
    <SvgIcon sx={{ width: "10px", height: "10px" }}>
      {/* credit: plus icon from https://heroicons.com/ */}
      <svg
        width="10"
        height="10"
        viewBox="0 0 10 10"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g id="vuesax/bold/tick-circle">
          <g id="tick-circle">
            <path
              id="Vector"
              d="M5.00004 0.833374C2.70421 0.833374 0.833374 2.70421 0.833374 5.00004C0.833374 7.29587 2.70421 9.16671 5.00004 9.16671C7.29587 9.16671 9.16671 7.29587 9.16671 5.00004C9.16671 2.70421 7.29587 0.833374 5.00004 0.833374ZM6.99171 4.04171L4.62921 6.40421C4.57087 6.46254 4.49171 6.49587 4.40837 6.49587C4.32504 6.49587 4.24587 6.46254 4.18754 6.40421L3.00837 5.22504C2.88754 5.10421 2.88754 4.90421 3.00837 4.78337C3.12921 4.66254 3.32921 4.66254 3.45004 4.78337L4.40837 5.74171L6.55004 3.60004C6.67087 3.47921 6.87087 3.47921 6.99171 3.60004C7.11254 3.72087 7.11254 3.91671 6.99171 4.04171Z"
              fill="#007AFF"
            />
          </g>
        </g>
      </svg>
    </SvgIcon>
  );
}
