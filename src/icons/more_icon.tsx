import * as React from "react";
import SvgIcon from "@mui/material/SvgIcon";

export default function MoreIcon() {
  return (
    <SvgIcon sx={{ width: "20px", height: "20px" }}>
      {/* credit: plus icon from https://heroicons.com/ */}
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="32"
        height="32"
        viewBox="0 0 24 24"
        fill="none"
      >
        <path
          stroke="#007AFF"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="1.5"
          d="M19.92 8.95l-6.52 6.52c-.77.77-2.03.77-2.8 0L4.08 8.95"
        ></path>
      </svg>
    </SvgIcon>
  );
}
