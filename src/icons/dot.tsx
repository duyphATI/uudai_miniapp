import * as React from "react";
import SvgIcon from "@mui/material/SvgIcon";

export default function DotIcon() {
  return (
    <SvgIcon sx={{ width: "5px", height: "5px" }}>
      {/* credit: plus icon from https://heroicons.com/ */}
      <svg
        width="6"
        height="6"
        viewBox="0 0 6 6"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle id="Ellipse 10" cx="3" cy="3" r="2.5" fill="#007AFF" />
      </svg>
    </SvgIcon>
  );
}
